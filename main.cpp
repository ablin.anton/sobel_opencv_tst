#include <iostream>

#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#define     LOG_IMG_ENABLE

using namespace std;
using namespace cv;

static void logImg( const cv::String& filename, cv::InputArray img);
static void customSobel(const Mat &src, Mat &dst, int dx, int dy);

int main( int argc, char** argv )
{
    CommandLineParser parser(argc, argv, "{@input | src.jpg | input image}"
                                             "{help    h|false|show help message}");
    parser.about("Sobel v1.0.0");
    if (parser.get<cv::String>("help") == cv::String("true"))
    {
        parser.printMessage();
        return 0;
    }

    try {
        const cv::String & src_file = parser.get<cv::String>( "@input" );
        Mat image = imread( samples::findFile( src_file ), IMREAD_COLOR );
        if (image.empty())
        {
            std::cout << "Could not open or find the image!\n" << std::endl;
            std::cout << "Usage: " << src_file  << " <Input image>" << std::endl;
            return EXIT_FAILURE;
        }
        imshow("original", image);

        Mat src, src_gray, dst;
        GaussianBlur(image, src, Size(1, 1), 0, 0, BORDER_DEFAULT);
        cvtColor(src, src_gray, COLOR_BGR2GRAY);

        logImg("./gauss_gray.jpg", src_gray);

        Mat grad;
        Mat grad_x, grad_y;
        Mat abs_grad_x, abs_grad_y;

        // orig Sobel
        Sobel(src_gray, grad_x, CV_16S, 1, 0);
        Sobel(src_gray, grad_y, CV_16S, 0, 1);

        //convert to CV_8U
        convertScaleAbs(grad_x, abs_grad_x);
        convertScaleAbs(grad_y, abs_grad_y);
        //sum
        addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);

        imshow("sobel orig", grad);
        logImg("./sobel_orig.jpg", grad);

        // custom Sobel
        customSobel(src_gray, grad_x, 1, 0);
        customSobel(src_gray, grad_y, 0, 1);

        //convert to CV_8U
        convertScaleAbs(grad_x, abs_grad_x);
        convertScaleAbs(grad_y, abs_grad_y);
        addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);

        imshow("sobel custom", grad);
        logImg("./sobel_custom.jpg", grad);

        waitKey(0);
    }
    catch( std::exception& e )
    {
        const char* err_msg = e.what();
        std::cout << "exception caught: " << err_msg << std::endl;
        cout << "Failed." << endl;
        return EXIT_FAILURE;
    }
    cout << "Complete." << endl;
    return 0;
}

/**
 * X-Direction Kernel
 *  -1 0 1
 *  -2 0 2
 *  -1 0 1
 */
template< typename T>
inline int xGradient(const Mat  &image, int x, int y)
{
    return image.at<T>(y-1, x-1) +
                2*image.at<T>(y, x-1) +
                 image.at<T>(y+1, x-1) -
                  image.at<T>(y-1, x+1) -
                   2*image.at<T>(y, x+1) -
                    image.at<T>(y+1, x+1);
}

/**
 * Y-Direction Kernel
 *  -1 -2 -1
 *   0  0  0
 *   1  2  1
 */
template< typename T>
inline int yGradient(const Mat  &image, int x, int y)
{
    return image.at<T>(y-1, x-1) +
                2*image.at<T>(y-1, x) +
                 image.at<T>(y-1, x+1) -
                  image.at<T>(y+1, x-1) -
                   2*image.at<T>(y+1, x) -
                    image.at<T>(y+1, x+1);
}

void customSobel(const cv::Mat & src, cv::Mat & dst, int dx, int dy)
{
    if( src.empty())
        return;

    if(src.type() != CV_8U) {
        throw std::logic_error(std::string{} + "Invalid type! Only CV_8U. Error in (" +
                            __func__ + ") " + __FILE__ + ":" + std::to_string(__LINE__));
    }

    dst = Mat::zeros( src.size(), CV_16S );

    for(int y = 1; y < src.rows - 1; y++){
        for(int x = 1; x < src.cols - 1; x++){
            int gx=0, gy=0;

            if(dx)
                gx = xGradient<uint8_t>(src, x, y);
            if(dy)
                gy = yGradient<uint8_t>(src, x, y);

            int16_t sum = gx + gy;
            dst.at<int16_t>(y,x) = sum;
        }
    }
}


void logImg( const cv::String& filename, cv::InputArray img)
{
#if defined LOG_IMG_ENABLE
    if(!imwrite(filename, img))
        std::cout << "Could not save the image!\n" << std::endl;
#else
    (void)filename;
    (void)img;
#endif
}
